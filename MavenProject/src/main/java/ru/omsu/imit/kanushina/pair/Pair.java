package ru.omsu.imit.kanushina.pair;

/**
 * Created by Student on 03.12.2016.
 */
public class Pair<T extends Comparable<T>> {
    private T first;
    private T second;


    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }

    public Pair(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public Pair() {
    }

    public T min() {
        int t = first.compareTo(second);
        if (t <= 0) {
            return first;
        } else {
            return second;
        }

    }

    public T max() {
        int t = first.compareTo(second);
        if (t <= 0) {
            return second;
        } else {
            return first;
        }
    }

    public <T extends Comparable<T>> Pair<T> elementMaxMin(T[] arr) {
        T min = arr[0];
        T max = arr[0];
        for (T anArr : arr) {
            if (max.compareTo(anArr) < 0) {
                max = anArr;
            }
            if (anArr.compareTo(min) < 0) {
                min = anArr;
            }
        }
        return new Pair<T>(min, max);
    }
}