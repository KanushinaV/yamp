package ru.omsu.imit.kanushina;

/**
 * Created by Student on 03.12.2016.
 */
public abstract class Fruit implements Comparable<Fruit> {
    public abstract double weight();
    public int compareTo(Fruit o) {
        return (weight() > o.weight()) ? 1 : ((weight() == o.weight()) ? 0 : -1);
    }
}
