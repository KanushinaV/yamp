package ru.omsu.imit.kanushina.pair;

/**
 * Created by Student on 03.12.2016.
 */
public abstract class Fruit implements Comparable<Fruit> {
    public double weight;
    public double getFruit(double weight){
        return weight;
    }
    public void setFruit(){
        this.weight = weight;
    }
    public int compareTo(Fruit o) {
        return (weight > o.weight) ? 1 : ((weight == o.weight) ? 0 : -1);
    }
}
