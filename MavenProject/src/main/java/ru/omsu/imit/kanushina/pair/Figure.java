package ru.omsu.imit.kanushina.pair;

/**
 * Created by Student on 03.12.2016.
 */
public abstract class Figure implements Comparable<Figure> {
    public double area;

    public double getFigure(double area){
        return area;
    }
    public void setFigure(){
        this.area=area;
    }


    public int compareTo(Figure o) {
        return (area > o.area) ? 1 : ((area == o.area) ? 0 : -1);
    }
}
