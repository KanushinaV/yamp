package ru.omsu.imit.kanushina.Vector;

/**
 * Created by Student on 22.10.2016.
 */
public class VectorArray {
    private Vector[] vectorsArray;
    private double[] lengths;

    public VectorArray(int length) {  //����������� �� ������� �����������
        vectorsArray = new Vector[length];
        lengths = new double[length];
        for (int i = 0; i < length; i++) {
            vectorsArray[i] = new Vector();
            lengths[i] = 0;
        }
    }

    public Vector get(int i) { // ������ ���������
        return vectorsArray[i];
    }

    public int lengthArray() { // ����� ����� �������
        return vectorsArray.length;
    }

    public void changingElement(int i, Vector v) { // ������ �-���� ��������
        vectorsArray[i] = v;
        lengths[i] = v.length();
    }

    public double maxLenghtVector() {// ����� ����� ������� ����� �������
        double max = 0;
        for (double l : lengths) {
            if (l > max) {
                max = l;
            }
        }
        return max;
    }

    public int searchVector(Vector vector) {
        //	����� ��������� ������� � ������� (��������� � ������ ������� ��������� ��� �1, ���� �� ������),
        for (int i = 0; i < vectorsArray.length; i++) {
            if (vector.equals(vectorsArray[i])) {
                return i;
            }
        }
        return -1;
    }

    public Vector sumAllVectors() { //����� ���� ��������
        Vector vector = new Vector();
        for (Vector v : vectorsArray) {
            vector = VectorProcessor.sum(vector, v);
        }
        return vector;
    }

    public Vector linear�(double[] a) {
        //�����, ������� �������� �� ���� ������ ������������ ����� (�������������)
        // ��������� �������� ���������� �������� � ��������� ��������������.
        // ��� ������������ ���� �������� �������� � �������������, ����������� ������� ������� ������
        Vector vector = new Vector();
        if (vectorsArray.length != a.length) {
            return vector;
        }
        for (int i = 0; i < a.length; i++) {
            vector = VectorProcessor.sum(vector, new Vector(a[i] * vectorsArray[i].getX(),
                    a[i] * vectorsArray[i].getY(),
                    a[i] * vectorsArray[i].getZ()));
        }
        return vector;
    }

    public Point[] arrayOfPoints(Point p) {
        // �����, ������� �������� �� ���� ����� P
        // ��������� ������ �����, ������ �� ������� � ��������� ������ ����� P, �� ��������������� ������.

        Point[] arrayPoints = new Point[vectorsArray.length];
        for (int i = 0; i < vectorsArray.length; i++) {
            arrayPoints[i] = new Point(p.getX() + vectorsArray[i].getX(),
                    p.getY() + vectorsArray[i].getY(),
                    p.getZ() + vectorsArray[i].getZ());
        }
        return arrayPoints;
    }

}
