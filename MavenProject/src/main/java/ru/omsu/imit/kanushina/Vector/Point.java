package ru.omsu.imit.kanushina.Vector;

import java.lang.Math;

/**
 * Created by Student on 01.10.2016.
 */
public class Point {
    private double x;
    private double y;
    private double z;

    public static final double EXP = 0.000001;


    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point() {
    }//�������� �� ��������� (0,0,0) - ������ ���������

    /**
     * Extracted coordinate variables x, y, z
     * @return
     */
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    /**
     * Set coordinate variables x, y, z
     * @param x
     */
    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.x = y;
    }

    public void setZ(double z) {
        this.x = z;
    }

    /**
     * displays the coordinates of the point to the console
     *
     * @param x coordinates x of point
     * @param y coordinates y of point
     * @param z coordinates z of point
     */
    public void showPoint(double x, double y, double z) {
        System.out.println(x + ", " + y + ", " + z);
    }

    /**
     * comparing the equality of two points
     *
     * @param a point A
     * @param b point B
     * @return true or false
     */
    public static boolean equalsPoint(Point a, Point b) {
        if (
                Math.abs(a.getX() - Math.abs(b.getX())) < EXP
                && Math.abs(a.getX() - Math.abs(b.getX())) < EXP
                && Math.abs(a.getX() - Math.abs(b.getX())) < EXP) {
            return true;
        } else {
            return false;
        }
    }
}
