package ru.omsu.imit.kanushina;

/**
 * Created by Student on 24.09.2016.
 */
public class TaylorProgramm {
    public static double fact(int a) {
        double answer;
        if (a == 0) {
            return (1);
        } else {
            answer = 1;
            for (int i = 1; i <= a; i++) {
                answer = answer * i;
            }
            return (answer);
        }
    }
    public static double exp(double power, double accuracy) {
        double f;
        int a = 0;
        double e = Math.E;
        double answer = 0;
        do {
            f = (Math.pow(e, 0) * Math.pow(power, a)) / fact(a);
            answer += f;
            a++;
        } while (Math.abs(f) > accuracy);
        return (answer);
    }
}
