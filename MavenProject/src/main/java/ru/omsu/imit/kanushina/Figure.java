package ru.omsu.imit.kanushina;

/**
 * Created by Student on 03.12.2016.
 */
public abstract class Figure implements Comparable<Figure> {
    public abstract double area();

    public int compareTo(Figure o) {
        return (area() > o.area()) ? 1 : ((area() == o.area()) ? 0 : -1);
    }
}
