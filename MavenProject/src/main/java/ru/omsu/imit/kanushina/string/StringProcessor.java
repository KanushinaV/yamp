package ru.omsu.imit.kanushina.string;

import java.util.HashMap;

/**
 * Created by Student on 12.11.2016.
 */
public class StringProcessor {
    public static String nString(String s, int n) {
        StringBuilder a = new StringBuilder();
        for (int i = 0; i < n; n++) {
            a.append(s);
        }
        return a.toString();
    }

    public static HashMap<String, Integer> statistics(String a) {
        String[] parts = a.split(" ");
        HashMap<String, Integer> statistics = new HashMap<String, Integer>();
        for (int i = 0; i < parts.length; i++) {
            if (statistics.containsKey(parts[i])) {
                int n = statistics.get(parts[i]);
                statistics.put(parts[i], statistics.get(parts[i]) + 1);
            } else {
                statistics.put(parts[i], 1);
            }
        }
        return statistics;
    }

    public static String change(String s) {
        return s.replace("1", "one").replace("2", "two").replace("3", "three");
    }

    public static StringBuilder removeEverySecond(StringBuilder s) {
        for (int i = 1; i < s.length(); i++) {
            s.deleteCharAt(i);
        }
        return s;
    }
}
