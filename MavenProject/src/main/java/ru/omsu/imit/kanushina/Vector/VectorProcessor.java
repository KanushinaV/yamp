package ru.omsu.imit.kanushina.Vector;

/**
 * Created by Student on 08.10.2016.
 */
public class VectorProcessor {
    public static final double EPS = 0.000001;
    public static Vector sum(Vector a, Vector b) {
        return  new Vector(a.getX() + b.getX(), a.getY() + b.getY(), a.getZ() + b.getZ());
    }

    public static Vector difference(Vector a, Vector b) {
        return new Vector(a.getX() - b.getX(), a.getY() - b.getY(), a.getZ() - b.getZ());
    }

    public static double scalarProduct(Vector a, Vector b) {
        return a.getX() * b.getX() + a.getY() * b.getY() + a.getZ() * b.getZ();
    }

    public static Vector crossProduct(Vector a, Vector b) {
        return new Vector(a.getY() * b.getZ() - a.getZ() * b.getY(),
                a.getZ() * b.getX() - a.getX() * b.getZ(),
                a.getX() * b.getY() - a.getY() * b.getX()
        );
    }
    public static boolean collinearity(Vector a, Vector b){
        return isZero(a.getX() / b.getX() - a.getY() / b.getY()) &&
                isZero(a.getY() / b.getY() - a.getZ() / b.getZ());
    }

    private static boolean isZero(double x) {
        return Math.abs(x) < EPS;
    }
}
