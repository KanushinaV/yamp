package ru.omsu.imit.kanushina;

import java.util.Scanner;
import java.lang.Math;

class Taylor {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double power;
        double accuracy;
        //double f;
        double answer;
        int a = 0;
        TaylorProgramm obj = new TaylorProgramm();
        System.out.println("Enter the power of EXP: ");
        power = sc.nextDouble();
        System.out.println("Enter the accuracy: ");
        accuracy = sc.nextDouble();
        answer = obj.exp(power, accuracy);
        System.out.println(answer);
    }
}
  