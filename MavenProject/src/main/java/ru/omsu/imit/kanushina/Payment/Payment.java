package ru.omsu.imit.kanushina.Payment;

/**
 * Created by Student on 12.11.2016.
 */
public class Payment {
    private String FIO;
    private int day;
    private int month;
    private int year;
    private int amount;

    public Payment(String FIO, int day, int month, int year, int amount) {
        this.FIO = getFIO();
        this.day = getDay();
        this.month = getMonth();
        this.year = getYear();
        this.amount = getAmount();

    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getFIO() {
        return FIO;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;

        Payment payment = (Payment) o;

        if (getDay() != payment.getDay()) return false;
        if (getMonth() != payment.getMonth()) return false;
        if (getYear() != payment.getYear()) return false;
        if (getAmount() != payment.getAmount()) return false;
        return !(getFIO() != null ? !getFIO().equals(payment.getFIO()) : payment.getFIO() != null);

    }

    @Override
    public int hashCode() {
        int result = getFIO() != null ? getFIO().hashCode() : 0;
        result = 31 * result + getDay();
        result = 31 * result + getMonth();
        result = 31 * result + getYear();
        result = 31 * result + getAmount();
        return result;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "FIO='" + FIO + '\'' +
                ", day=" + day +
                ", month=" + month +
                ", year=" + year +
                ", amount=" + amount +
                '}';
    }
}
