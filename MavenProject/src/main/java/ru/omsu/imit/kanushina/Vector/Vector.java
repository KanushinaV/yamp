package ru.omsu.imit.kanushina.Vector;

/**
 * Created by Student on 01.10.2016.
 */
public class Vector {
    private double x;
    private double y;
    private double z;
    public static final double EPS = 0.000001;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector(Point a, Point b) {
        x = a.getX() - b.getX();
        y = a.getY() - b.getY();
        z = a.getZ() - b.getZ();
    }

    public Vector() {
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.x = y;
    }

    public void setZ(double z) {
        this.x = z;
    }

    public void printVector(double x, double y, double z) {
        System.out.println(x + ", " + y + ", " + z);
    }



    public double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector vector = (Vector) o;
        return Math.abs(x - vector.getX()) < EPS
                && Math.abs(y - vector.getY()) < EPS
                && Math.abs(z - vector.getZ()) < EPS;

    }

}
