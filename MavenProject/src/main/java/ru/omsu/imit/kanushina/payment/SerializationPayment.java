package main.java.ru.omsu.imit.kanushina.payment;

import java.io.*;

/**
 * Created by Student on 24.12.2016.
 */
public class SerializationPayment{
    //������������ � �����
    public static void serializationTxt(Pyment payment, File file) throws IOException{
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            String str = payment.getFIO()+";"
                    +payment.getDay()+";"
                    +payment.getMonth()+";"
                    +payment.getYear()+";"
                    +payment.getAmount();
            bufferedWriter.write(str);
        }
    }
    public static Payment deserializationTxt(File file) throws IOException{
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))){
            String s = bufferedReader.readLine();
            String[] str = s.split(";");
            return new Payment(str[0],
                    Integer.valueOf(str[1]),
                    Integer.valueOf(str[2]),
                    Integer.valueOf(str[3]),
                    Integer.valueOf(str[4]));
        }
    }

    public static void serialization(Payment payment, File file) throws IOException, ClassNotFoundException{
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))){
            oos.writeObject(payment);
        }
    }

    public static Payment deserialization(File file) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){
           Payment payment;
            payment = (Payment)ois.readObject();
            return payment;
        }
    }
}
