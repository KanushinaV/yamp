package ru.omsu.imit.kanushina.payment;

import java.io.*;

/**
 * Created by Student on 24.12.2016.
 */
public class SerializationArray {

    public static void writeBytes(int[] array, File file) throws IOException {
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(file))) {
            out.writeInt(array.length);
            for (int i : array) {
                out.writeInt(i);
            }
        }
    }

    public static int[] readBytes(File file) throws IOException {
        try (DataInputStream in = new DataInputStream(new FileInputStream(file))) {
            int size = in.readInt();
            int[] result = new int[size];
            for (int i = 0; i < size; i++) {
                result[i] = in.readInt();
            }
            return result;
        }
    }

    public static void writeText(int[] array, File file) throws IOException {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
            for (int i = 0; i < array.length - 1; i++) {
                out.write(array[i] + ", ");
            }
            out.write(Integer.toString(array[array.length - 1]));
        }
    }

    public static int[] readText(File file) throws IOException {
        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
            String[] numbers = in.readLine().split(",");

            int size = numbers.length;
            int[] result = new int[size];

            for (int i = 0; i < size; i++) {
                result[i] = Integer.parseInt(numbers[i].trim());
            }

            return result;
        }
    }
}
