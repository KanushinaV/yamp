package ru.omsu.imit.kanushina;
import org.junit.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Student on 01.10.2016.
 */

public class TestPoint {
    @Test
    public void testingPoint (){
        Point a = new Point(1,2,3);
        Point b = new Point(4,3,7);
        boolean value = Point.equalsPoint(a,b);
        assertEquals(value, false);
    }
    @Test
    public void testingPoint2 (){
        Point a = new Point(1,2,3);
        Point b = new Point(1,2,3);
        boolean value = Point.equalsPoint(a,b);
        assertEquals(value, true);
    }
}
