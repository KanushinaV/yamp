package ru.omsu.imit.kanushina;
import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Student on 17.09.2016.
 */
public class TestTaylor{
    @Test
    public void positiveTaylorTest(){
        double Test = TaylorProgramm.exp(2, 0.01);
        assertEquals(Test,Math.exp(2), 0.01);
    }
    public void negativeTaylorTest(){
        double Test = TaylorProgramm.exp(-2,0.0001);
        assertEquals(Test,Math.exp(-2), 0.0001);

    }
}
