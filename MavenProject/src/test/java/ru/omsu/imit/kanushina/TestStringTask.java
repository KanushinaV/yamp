package ru.omsu.imit.kanushina;

import org.junit.Test;
import ru.omsu.imit.kanushina.string.StringTask;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by Student on 22.10.2016.
 */
public class TestStringTask {
    @Test
    public void testTurnString(){
        String s = "123456";
        String s1 = StringTask.turnString(s);
        String r = "654321";
        assertEquals(r,s1);
    }
    @Test
    public void testConversionArrStr(){
        int[] arr = {8,5,6,2,1,3,4};
        String r = "8562134";
        String s = StringTask.conversionArrStr(arr);
        assertEquals(r, s);
    }
    @Test
    public void testConversionStrArr(){
        int[] arr = {8,5,6,2,1,3,4};
        String s = "8 5 6 2 1 3 4";
        int[] sarr = StringTask.conversionStrArr(s);
        assertArrayEquals(arr,sarr);

    }

}
