package MavenProject.src.test.java.ru.omsu.imit.kanushina;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import ru.omsu.imit.kanushina.payment.Payment;
import ru.omsu.imit.kanushina.payment.SerializationArray;
import ru.omsu.imit.kanushina.payment.SerializationPayment;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Student on 24.12.2016.
 */
public class TestSerialization {
    @Rule
    public static TemporaryFolder folder= new TemporaryFolder();
    @Test
    public void testSTP() throws IOException {
        Payment payment = new Payment("Kanushiuna", 9, 10, 1997, 1520);
        File file = folder.newFile();
        SerializationPayment.serializationTxt(payment,file);
        Payment payment2 = SerializationPayment.deserializationTxt(file);
        assertEquals(payment, payment2);

    }

    @Test
    public static void testSBP() throws IOException, ClassNotFoundException {
        Payment payment = new Payment("Kanushiuna", 9, 10, 1997, 1520);
        File file = folder.newFile();
        SerializationPayment.serialization(payment, file);
        Payment payment1 = SerializationPayment.deserialization(file);
        assertEquals(payment, payment1);
    }
    @Test
    public static void testSBA() throws IOException {
        int arrSize = 1 + (int) (Math.random() * 1000000);
        int[] arr = new int[arrSize];
        for (int i = 0; i < arrSize; i++) {
            arr[i] = (int) (Math.random() * Integer.MAX_VALUE);
        }
        File file = folder.newFile();
        SerializationArray.writeText(arr, file);
        int[] newArr = SerializationArray.readText(file);
        assertArrayEquals(arr, newArr);
    }
    @Test
    public static void testSTA() throws IOException {
        int[] arr = new int[] {29, 13, 666, 999, 26};

        File file = folder.newFile();
        SerializationArray.writeBytes(arr, file);
        int[] newArr = SerializationArray.readBytes(file);

        assertArrayEquals(arr, newArr);
    }

}
