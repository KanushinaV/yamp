package ru.omsu.imit.kanushina;
import org.junit.Test;
import ru.omsu.imit.kanushina.pair.Apple;
import ru.omsu.imit.kanushina.pair.Fruit;
import ru.omsu.imit.kanushina.pair.Pair;
import ru.omsu.imit.kanushina.pair.Pear;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by Student on 17.12.2016.
 */
public class TestPair {
    @Test
    public void testMinFruit(){
        Apple apple = new Apple(10);
        Pear pear = new Pear(5);
        Pair<Fruit> pair = new Pair<Fruit>(apple, pear);
        assertEquals(pear, pair.min());
    }

    @Test
    public void testMaxFruit(){
        Apple apple = new Apple(10);
        Pear pear = new Pear(5);
        Pair<Fruit> pair = new Pair<Fruit>(apple, pear);
        assertEquals(apple, pair.max());
    }
    @Test
    public void testMassFruit(){
        Fruit[] F1 = {new Pear(30), new Apple(50)};
        Fruit[] F2 = {new Apple(50), new Pear(30)};


    }
}
