package ru.omsu.imit.kanushina;
import org.junit.*;
import ru.omsu.imit.kanushina.Vector.Point;
import ru.omsu.imit.kanushina.Vector.Vector;

import static org.junit.Assert.*;

/**
 * Created by Student on 08.10.2016.
 */

public class TestVector {
    @Test
    public void testLengthVector(){
        Vector a = new Vector(2,2,1);
        double value = a.length();
        assertEquals(3,value, 0.001);
    }
    @Test
    public void testeEqualityVectorNo(){
        Vector a = new Vector(1, 1, 1);
        Vector b = new Vector(5, 2, 3);
        assertFalse(a.equals(b));
    }
    @Test
    public void testeEqualityVectorYes(){
        Vector a = new Vector(1, 2, 3);
        Vector b = new Vector(1, 2, 3);
        assertTrue(a.equals(b));
    }
    @Test
    public void testTwoPoint(){
        Point a = new Point(3,6,10);
        Point b = new Point(1,3,4);
        Vector vector = new Vector(a,b);
        Vector value = new Vector(2,3,6);
        assertTrue(vector.equals(value));
    }
}
