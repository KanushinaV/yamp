package ru.omsu.imit.kanushina;

import org.junit.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Student on 24.09.2016.
 */
public class TestMass {
    @Test
    public void testingSumMass() {
        double[] Mass = {1, 2, 3, 4, 5};
        double sum = mass.sumMass(Mass);
        assertEquals(15, sum, 0.001);
    }

    @Test
    public void testingEvenMass() {
        double[] Mass = {1, 2, 3, 4, 5};
        double even = mass.evenMass(Mass);
        assertEquals(2, even, 0.001);
    }

    @Test
    public void testingSegment1Mass() {
        double[] Mass = {1, 2, 3, 4, 5};
        double segment1 = mass.segmentMass(Mass, 0, 6);
        assertEquals(5, segment1, 0.001);
    }

    @Test
    public void testingSegment2Mass() {
        double[] Mass = {1, 2, 3, 4, 5};
        double segment2 = mass.segmentMass(Mass, -88, -3);
        assertEquals(0, segment2, 0.001);
    }

    @Test
    public void testingSegment3Mass() {
        double[] Mass = {1, 2, 3, 4, 5};
        double segment3 = mass.segmentMass(Mass, -1, 3);
        assertEquals(2, segment3, 0.001);
    }

    @Test
    public void testingPositiveMass() {
        double[] Mass = {1, 2, 3, 4, 5};
        boolean positive = mass.positiveMass(Mass);
        assertEquals(true, positive);
    }

    @Test
    public void testingBackMass() {
        double[] Mass = {1, 2, 3, 4, 5};
        double[] Mass2 = mass.backMass(Mass);
        double[] BackMass = {5, 4, 3, 2, 1};
        assertArrayEquals(BackMass, Mass2, 0.001);
    }

}
