package ru.omsu.imit.kant;

import java.util.Scanner;

/**
 * Created by Student on 19.11.2016.
 */
public class Combination {
    public static StringBuilder mergerString(String[] a, String[] b){
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < a.length; i++) {
            if (i < b.length) {
                result.append(a[i]).append(" ").append(b[i]).append(" ");
            } else {
                result.append(a[i]).append(" ");
            }
        }
        return result;
    }
    public static String combination(String a, String b) {
        StringBuilder result;
        new StringBuilder();
        String[] partsA = a.split(" ");
        String[] partsB = b.split(" ");
        if (partsA.length > partsB.length) {
            result = Combination.mergerString(partsA, partsB);
        } else {
            result = Combination.mergerString(partsB, partsA);
        }
        return result.toString().substring(0, result.length() - 1);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the first string");
        String a = sc.nextLine();
        System.out.println("Enter the second string");
        String b = sc.nextLine();
        System.out.println("Output: " + Combination.combination(a, b));
    }
}
