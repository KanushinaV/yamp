package ru.omsu.imit.kant;

import org.junit.Test;
import ru.omsu.imit.kant.Combination;

import static org.junit.Assert.assertEquals;

/**
 * Created by Student on 19.11.2016.
 */
public class TestCombination {
    @Test
    public void testCombination1() {
        String a = "1 2 3 4 5";
        String b = "one two three four five";
        String result = "1 one 2 two 3 three 4 four 5 five";
        String process = Combination.combination(a, b);
        assertEquals(result, process);
    }

    @Test
    public void testCmbination2() {
        String a = "1 2 3 4 5";
        String b = " ";
        String result = "1 2 3 4 5";
        String process = Combination.combination(a, b);
        assertEquals(result, process);

    }

    @Test
    public void testCmbination3() {
        String a = "1 2 3 4 5";
        String b = "1 2 3 4 5 6 7 8";
        String result = "1 1 2 2 3 3 4 4 5 5 6 7 8";
        String process = Combination.combination(a, b);
        assertEquals(result, process);
    }
}
